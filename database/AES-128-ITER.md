<div align="center">

| Number of rounds | Iterated <br> differential characteristic | Probability <br> of the iterated characteristic | Optimal <br> probability |
| ----------: | ---------------------------------: | :----------------: | :--------------------------: |
|  1 | ``` 0 -> 0xC73200C43BAB000000000000D6007ED6``` <br> ``` 1 -> 0xC73200C43BAB000000000000D6007ED6``` | 2^{-52} | &check; |
|  2 | ``` 0 -> 0xB8CA002EE29300000000000065003965``` <br> ``` 1 -> 0x005E00394BE300000000000065003965``` <br> ``` 2 -> 0xB8CA002EE29300000000000065003965``` | 2^{-98} | &check; |

</div>

<!-- AIGER INPUTS
1 [0xC73200C43BAB000000000000D6007ED6, 0xA52A0090900100000000000054009095]
2 [0xB8CA002EE29300000000000065003965, 0xCE1700A8A8170000000000002E00A8BE, 0x0017007171720000000000002E00712E]
-->
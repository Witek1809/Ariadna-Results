<div align="center">

| Number of rounds | Iterated <br> differential characteristic | Probability <br> of the iterated characteristic | Optimal <br> probability |
| ----------: | ---------------------------------: | :----------------: | :--------------------------: |
|  1 | ``` 0 -> 0xB808B8089080908089888988``` <br> ``` 1 -> 0xB808B8089080908089888988``` | 2^{-28} | &check; |

</div>

<!-- AIGER INPUTS
1 [0xB808B8089080908089888988, 0x880888083188318839803980]
2 
-->
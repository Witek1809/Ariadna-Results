# Ariadna-Results

The database of differential characteristics determined by the software implemented as part of the doctoral dissertation entitled "Methods of automated search for differential characteristics in relation to cryptanalysis of block ciphers".

## Database

<div align="center">

| Algorithm name | Block Size | Kind of characteristic        | Database link                                           |
| :------------- | ---------: | :---------------------------- | :-----------------------------------------------------: |
| AES            |        128 | Differential Classic          | [Show characteristics](./database/AES-128.md)           |
| AES            |        128 | Iterated Differential Classic | [Show characteristics](./database/AES-128-ITER.md)      |
| AES            |        128 | Differential Truncuted        | [Show characteristics](./database/AES-128-TRUNC.md)     |
| KLEIN          |         64 | Differential Classic          | [Show characteristics](./database/KLEIN-64.md)          |
| KLEIN          |         64 | Differential Truncuted        | [Show characteristics](./database/KLEIN-64-TRUNC.md)    |
| MIDORI         |         64 | Differential Classic          | [Show characteristics](./database/MIDORI-64.md)         |
| MIDORI         |         64 | Differential Truncuted        | [Show characteristics](./database/MIDORI-64-TRUNC.md)   |
| SATURNIN       |        256 | Differential Classic          | [Show characteristics](./database/SATURNIN-256.md)      |
| SATURNIN       |        256 | Iterated differential Classic |                                                         |
| SIMON          |         32 | Differential Classic          | [Show characteristics](./database/SIMON-32.md)          |
| SIMON          |         48 | Differential Classic          | [Show characteristics](./database/SIMON-48.md)          |
| SIMON          |         64 | Differential Classic          |                                                         |
| SIMON          |         96 | Differential Classic          |                                                         |
| SIMON          |        128 | Differential Classic          |                                                         |
| SPECK          |         32 | Differential Classic          | [Show characteristics](./database/SPECK-32.md)          |
| SPECK          |         48 | Differential Classic          |                                                         |
| SPECK          |         64 | Differential Classic          |                                                         |
| SPECK          |         96 | Differential Classic          | [Show characteristics](./database/SPECK-96.md)          |
| SPECK          |        128 | Differential Classic          | [Show characteristics](./database/SPECK-128.md)         |
| PRESENT        |         64 | Differential Classic          | [Show characteristics](./database/PRESENT-64.md)        |
| PRESENT        |         64 | Differential Truncuted        |                                                         | 
| PYJAMASK       |         96 | Differential Classic          | [Show characteristics](./database/PYJAMASK-96.md)       |
| PYJAMASK       |         96 | Iterated Differential Classic | [Show characteristics](./database/PYJAMASK-96-ITER.md)  |
| PYJAMASK       |        128 | Differential Classic          | [Show characteristics](./database/PYJAMASK-128.md)      |
| PYJAMASK       |        128 | Iterated Differential Classic |                                                         |

</div>
